# mixins.py
# mixins.py
from django.contrib.auth.mixins import UserPassesTestMixin
from .models import buylist

class OnlyYouMixin(UserPassesTestMixin):
    raise_exception = True

    def test_func(self):
        user = self.request.user
        detail=buylist.objects.filter(id=self.kwargs['pk']).first()#欲しいものの作成者探し
        return user.pk == detail.user_id