from accounts.models import CustomUser
from django.db import models

# Create your models here.


class buylist (models.Model):

    PRIORITY_CHOICES = (
        ("高", "高"),
        ("中", "中"),
        ("低", "低"),
    )



    
    user = models.ForeignKey(CustomUser,verbose_name='ユーザー',on_delete=models.PROTECT)
    good_name = models.CharField(verbose_name="品名", max_length=50, )
    good_price = models.IntegerField(verbose_name="価格", blank=True, null=True)
    good_quantity = models.IntegerField(verbose_name="個数", blank=True, null=True, default=1)
    good_deadline = models.DateField(verbose_name="購入予定日", auto_now=False, auto_now_add=False)
    good_purohase_data = models.DateField(verbose_name="購入日", auto_now=False, auto_now_add=False, blank=True, null=True)
    good_status = models.BooleanField(blank=True, null=True, default=True)
    good_create_data = models.DateTimeField(verbose_name="作成日時", auto_now_add=True)
    good_purohace_shop = models.CharField(verbose_name='購入店舗', max_length=50, blank=True, null=True)
    good_update_data = models.DateTimeField(verbose_name='更新日時',auto_now=True, auto_now_add=False)
    good_prionty = models.CharField(verbose_name='優先度',max_length=1, choices=PRIORITY_CHOICES,default='低')
    
    
    
    
    
    class Meta:
        verbose_name_plural = 'buylist'

    def __str__(self):
        return self.good_name