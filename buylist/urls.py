from .import views
from django.urls import path

app_name = 'buylist'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('inquiry/', views.InquiryView.as_view(), name='inquiry'),
    path('buylist-list/', views.BuylistListView.as_view(), name='buylist_list'),
    # path('buylist-list/<int:cote>/>', views.BuylistListView.as_view(), name='buylist_list_c'),
    path('buylist-detail/<int:pk>/', views.BuylistDetailView.as_view(), name='buylist_detail'),
    path('buylist-create/', views.BuylistCreateView.as_view(), name='buylist_create'),
    path('buylist-update/<int:pk>/', views.BuylistUpdateView.as_view(), name='buylist_update'),
    path('buylist-delete/<int:pk>/', views.BuylistDeleteView.as_view(), name='buylist_delete'),
    path('redirect/<int:pk>/', views.Swichbyflag.as_view(), name='redirect'),
]