from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import generic
from django.contrib import messages
from datetime import date, datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from .mixins import OnlyYouMixin
import logging
from .models import buylist
from .forms import InquiryForm , BuylistCreateForm

logger= logging.getLogger(__name__)

class IndexView(generic.TemplateView):
    template_name = "index.html"

class InquiryView(generic.TemplateView):
    template_name = "inquiry.html"
    form_class = InquiryForm
    success_url = reverse_lazy('buylist:inquiry')

    def form_valid(self,form):
        form.send_email()
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)



class BuylistListView(LoginRequiredMixin,generic.ListView):
    model = buylist
    template_name =  "buylist_list.html"
    paginate_by = 50

    # def get(self, request, *args, **kwargs):
    #     category_data = self.kwargs['']
    #     memos = Memo.objects.filter(user=self.request.user, good_status=category_data).order_by('good_deadline')
    #     return memos
    def get_queryset(self, **kwargs):
        # category_data = self.kwargs
        # print(category_data)
        buylists = buylist.objects.filter(user=self.request.user).order_by('-good_status', 'good_deadline', 'good_prionty', 'good_name')
        return buylists

class BuylistDetailView(OnlyYouMixin,generic.DetailView):
    model = buylist
    template_name = 'buylist_detail.html'
    # pk_url_kwarg = 'id'

class BuylistCreateView(LoginRequiredMixin,generic.CreateView):
    model = buylist
    template_name = 'buylist_create.html'
    form_class = BuylistCreateForm
    success_url = reverse_lazy('buylist:buylist_list')

    def form_valid(self, form):
        buylist = form.save(commit=False)
        buylist.user = self.request.user
        buylist.save()
        messages.success(self.request,"欲しいものを追加しました。")
        return super().form_valid(form)
        
    def form_invalid(self, form):
        messages.error(self.request,"欲しいものの追加に失敗しました")

class BuylistUpdateView(OnlyYouMixin,generic.UpdateView):
    model = buylist
    template_name = 'buylist_update.html'
    form_class = BuylistCreateForm

    def get_success_url(self):
        return reverse_lazy('buylist:buylist_detail',kwargs={'pk':self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request,'欲しいものを更新しました')
        return super().form_valid(form)

    def form_vallid(self, form):
        messages.success(self.request,'欲しいものの更新を失敗しました')
        return super().form_valid(form)

class BuylistDeleteView(OnlyYouMixin, generic.DeleteView):
    model =  buylist
    template_name = 'buylist_delete.html'
    success_url = reverse_lazy('buylist:buylist_list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request,"欲しいものを削除しました。")
        return super().delete(request,*args,**kwargs)

class Swichbyflag(generic.RedirectView):
    url = reverse_lazy('buylist:buylist_list')
#    messages.success(self.request,'リダイレクト成功')
    
    def get(self, request, *args, **kwargs):
        url = self.get_redirect_url(*args, **kwargs)
        
        id = kwargs['pk']
        s = buylist.objects.filter(pk=id).first()
        if s.good_status == 1 :
            s.good_status = 0
            s.good_purohase_data = date.today()
        elif s.good_status == 0 :
            s.good_status = 1
            s.good_purohase_data = None
        
        s.save()
        return redirect(url)

myview = BuylistListView.as_view()